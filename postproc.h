#ifndef POSTPROC_H
#define POSTPROC_H

#include <QMainWindow>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QRadioButton>
#include <QSpinBox>
#include <QTableWidget>
#include <QLineEdit>
#include <QLabel>

#include "backend.h"
#include "litecanvas.h"

class Postproc : public QMainWindow
{
    Q_OBJECT

public:
    Postproc( QWidget *parent = 0 );
    ~Postproc();

public slots:
    void refillTable(int n);
    void curXChanged(double x);
    void setMode();
    void updateAll();
    void saveResult();
    void s_exit();
    double getSumL();
    int getRodIndex(double x);
    double getY(GraphMode m, int i_rod, double x);
    double Nx(int p, double x);
    double Sx(int p, double x);
    double Ux(int p, double x);


protected:
    void resizeEvent( QResizeEvent* e );

private:
    Backend* back;

    GraphMode       mode;
    QWidget*        mainWid;
    QMenu*          fileMenu;
    LiteCanvas*     canvas;
    QHBoxLayout*    mainl;
    QVBoxLayout*    ctrll;
    QHBoxLayout*    currl;
    QHBoxLayout*    checkl;
    QVBoxLayout*    xl;
    QVBoxLayout*    yl;
    QRadioButton*   m1;
    QRadioButton*   m2;
    QRadioButton*   m3;
    QLabel*         xLbl;
    QLabel*         yLbl;
    QLabel*         xyLbl;
    QDoubleSpinBox* currX;
    QLineEdit*      currY;
    QSpinBox*       stepBox;
    QLabel*         rodNLab;
    QLabel*         stepLab;
    QSpinBox*       rodNumber;
    QTableWidget*   valuesTable;
};

#endif // WIDGET_H
