#include "types.h"

QDataStream& operator <<( QDataStream& out, const Rod& r ){
    out << r.L << r.A << r.E << r.maxF;
    return out;
}
QDataStream& operator >>( QDataStream& in, Rod& r ){
    in >> r.L >> r.A >> r.E >> r.maxF;
    return in;
}
