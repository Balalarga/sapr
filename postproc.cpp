#include "postproc.h"
#include <QFileDialog>
#include <QHeaderView>
#include <QMenuBar>
#include <QDebug>

Postproc::Postproc( QWidget *parent )
    : QMainWindow ( parent ),
      back        ( Backend::getInstance() ),
      mode        ( Mode_Nx ),
      mainWid     ( new QWidget()),
      canvas      ( new LiteCanvas( Mode_Nx,this) ),
      mainl       ( new QHBoxLayout(mainWid) ),
      ctrll       ( new QVBoxLayout() ),
      currl       ( new QHBoxLayout() ),
      checkl      ( new QHBoxLayout() ),
      xl          ( new QVBoxLayout() ),
      yl          ( new QVBoxLayout() ),
      m1          ( new QRadioButton(this) ),
      m2          ( new QRadioButton(this) ),
      m3          ( new QRadioButton(this) ),
      xLbl        ( new QLabel(" x ")),
      yLbl        ( new QLabel(" N\u2093 ")),
      xyLbl       ( new QLabel("Глобальные значения")),
      currX       ( new QDoubleSpinBox ),
      currY       ( new QLineEdit() ),
      stepBox     ( new QSpinBox() ),
      rodNLab     ( new QLabel("Номер стержня")),
      stepLab     ( new QLabel("Количество разбиений")),
      rodNumber   ( new QSpinBox()),
      valuesTable ( new QTableWidget() )
{
    rodNumber->setMinimum(1);
    setCentralWidget( mainWid );
    fileMenu = menuBar()->addMenu("Файл");
    fileMenu->addAction("Сохранить отчёт", this, SLOT(saveResult()), Qt::CTRL + Qt::Key_S);
    fileMenu->addSeparator();
    fileMenu->addAction("Выход",           this, SLOT(s_exit()),     Qt::ALT  + Qt::Key_F4);

    connect( currX, SIGNAL(valueChanged(double)), this, SLOT(curXChanged(double)));
    connect( stepBox, SIGNAL(valueChanged(int)), this, SLOT(refillTable(int)) );
    connect( canvas, SIGNAL(lineMoved(double)), currX, SLOT(setValue(double)) );
    connect( rodNumber, SIGNAL(valueChanged(int)), this,SLOT(updateAll()) );
    connect( m1, SIGNAL(clicked()), this, SLOT(setMode()) );
    connect( m2, SIGNAL(clicked()), this, SLOT(setMode()) );
    connect( m3, SIGNAL(clicked()), this, SLOT(setMode()) );

    mainl->addWidget( canvas );
    mainl->addLayout( ctrll );
    ctrll->addLayout( checkl );
    ctrll->addWidget( xyLbl );
    ctrll->addLayout( currl );
    ctrll->addWidget( rodNLab );
    ctrll->addWidget( rodNumber );
    ctrll->addWidget( stepLab );
    ctrll->addWidget( stepBox );
    ctrll->addWidget( valuesTable );

    xl->addWidget( xLbl );
    xl->addWidget( currX );
    yl->addWidget( yLbl );
    yl->addWidget( currY );
    currl->addLayout( xl );
    currl->addLayout( yl );
    currY->setReadOnly( true );
    currX->setDecimals(4);

    checkl->addWidget( m1 );
    checkl->addWidget( m2 );
    checkl->addWidget( m3 );

    stepBox->setValue( 10 );
    valuesTable->setRowCount( stepBox->value() );
    valuesTable->setColumnCount(2);
    valuesTable->setHorizontalHeaderLabels(QStringList()<<"x"<<"N\u2093");
    valuesTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    m1->setChecked( true );
    m1->setText   ( "N\u2093" );
    m2->setText   ( "U\u2093" );
    m3->setText   ( "\u03C3\u2093" );

}
Postproc::~Postproc()
{
}

double Postproc::getSumL(){
    double sum = 0;
    for(int i = 0; i < back->getRods().size(); i++){
        sum += back->getRods()[i].L;
    }
    return sum;
}
double Postproc::Nx( int p, double x ){
    Rod c = back->getRods()[p];
    QVector<double> u = back->getU();
    return c.A*c.E*(u[p+1] - u[p])/c.L + back->getDforces()[p]*c.L*(1-2*x/c.L) / 2.;
}
double Postproc::Ux( int p, double x ){
    Rod c = back->getRods()[p];
    QVector<double> u = back->getU();
    double df = back->getDforces()[p];
    return u[p] + x*(u[p+1]-u[p])/c.L + df*c.L*x*(1-x/c.L)/(2*c.E*c.A);
}
double Postproc::Sx( int p, double x ){
    Rod c = back->getRods()[p];
    return Nx(p, x)/c.A;
}
void Postproc::setMode(){
    if( m1->isChecked() ){
        canvas->setMode( Mode_Nx );
        mode = Mode_Nx;
        valuesTable->setHorizontalHeaderLabels(QStringList()<<"x"<< "N\u2093" );
        yLbl->setText( " N\u2093 " );
    }
    else if( m2->isChecked() ){
        canvas->setMode( Mode_Ux );
        mode = Mode_Ux;
        valuesTable->setHorizontalHeaderLabels(QStringList()<<"x"<<"U\u2093");
        yLbl->setText( " U\u2093 " );
    }
    else{
        canvas->setMode( Mode_Sx );
        mode = Mode_Sx;
        yLbl->setText( " \u03C3\u2093 " );
        valuesTable->setHorizontalHeaderLabels(QStringList()<<"x"<<"\u03C3\u2093");
    }
    canvas->update();
    updateAll();
}

double Postproc::getY( GraphMode m, int i_rod, double x){
    double sum = 0;
    if( m == Mode_Nx)
        return Nx(i_rod, x-sum);
    else if( m == Mode_Sx)
        return Sx(i_rod, x-sum);
    else
        return Ux(i_rod, x-sum);
    return 0;
}

void Postproc::saveResult()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Сохранить результат"), "", tr("RES(*.res)"));
    if(!fileName.contains(".res"))
        fileName += ".res";
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly)){
        return;
    }
    QTextStream out(&file);
    out.setFieldWidth(7);
    out<<"Nx";
    out<<"\n\r";
    for(int j = 0; j < back->getRods().size(); j++){
        double step = getSumL()/(stepBox->value());
        for(int i = 0; i < stepBox->value()+1; i++){
            out<<"|"<<i*step<<"|"<<getY(Mode_Nx, j, i*step)<<"|\n\r";
        }
        out<<"\n\r";
        out<<"Sx";
        out<<"\n\r";
        for(int i = 0; i < stepBox->value()+1; i++){
            out<<"|";
            out<<i*step<<"|";
            out<<getY(Mode_Sx, j, i*step)<<"|\n\r";
        }
        out<<"\n\r";
        out<<"Ux";
        out<<"\n\r";
        for(int i = 0; i < stepBox->value()+1; i++){
            out<<"|"<<i*step<<"|"<<getY(Mode_Ux, j, i*step)<<"|\n\r";
        }
    }
}

void Postproc::s_exit()
{
    exit(0);
}

void Postproc::resizeEvent( QResizeEvent *e )
{
    canvas->setFixedWidth( this->width() * .6 );
}

void Postproc::curXChanged(double x){
    double sum = 0;
    int index = 0;
    for(int i = 0; i < back->getRods().size(); i++){
        if( x <= sum+back->getRods()[i].L){
            index = i;
            break;
        }
        sum += back->getRods()[i].L;
    }
    currY->setText(QString::number(getY(mode, index, x)));
    canvas->setLineX(x);
}

void Postproc::updateAll(){
    currX->setMaximum(getSumL());
    rodNumber->setMaximum(back->getRods().size());
    refillTable(stepBox->value());
    curXChanged(currX->value());
    valuesTable->update();
}

void Postproc::refillTable( int n )
{
    if( !back->getU().size()){
        return;
    }
    n++;
    valuesTable->setRowCount(n);
    double step = back->getRods()[rodNumber->value()-1].L/(n-1);
    for(int i = 0; i < valuesTable->rowCount(); i++){
        double x = i*step;
        QTableWidgetItem* it1 = valuesTable->item(i,0);
        if( !it1 ){
            it1 = new QTableWidgetItem();
            valuesTable->setItem(i, 0, it1);
        }
        if(valuesTable->rowCount() == 1)
            x = 0;
        it1->setText(QString::number(x));
        auto it2 = valuesTable->item(i,1);
        if( !it2 ){
            it2 = new QTableWidgetItem();
            valuesTable->setItem(i, 1, it2);
        }
        it2->setText(QString::number(getY(mode, rodNumber->value()-1, x)));
        if( mode == Mode_Sx){
            int index = getRodIndex(x);
            if( qAbs(it2->text().toDouble()) >  back->getRods()[index].maxF){
                it1->setBackgroundColor(Qt::red);
                it2->setBackgroundColor(Qt::red);
            }else {
                it1->setBackground(Qt::white);
                it2->setBackground(Qt::white);
            }
        }else{
            it1->setBackground(Qt::white);
            it2->setBackground(Qt::white);
        }
    }
}

int Postproc::getRodIndex(double x){
    double sum = 0;
    for(int i = 0; i < back->getRods().size(); i++){
        if( x <= sum + back->getRods()[i].L )
            return i;
        sum += back->getRods()[i].L;
    }
}
