#ifndef LITECANVAS_H
#define LITECANVAS_H

#include <QWidget>
#include <QPainter>
#include <QPainterPath>

#include "backend.h"

class LiteCanvas : public QWidget
{
    Q_OBJECT
public:
    explicit LiteCanvas( GraphMode m = Mode_Nx, QWidget *parent = nullptr );
    ~LiteCanvas        ();

signals:
    void lineMoved(double x);

public slots:
    void setMode(GraphMode m);
    void setLineX(double x);

protected:
    double getMaxA        ();
    double getSumL        ();
    double getModify      ( double parem );
    double Nx             ( int n, double x );
    double Ux             ( int n, double x );
    double Sx             ( int n, double x );
    void drawRods         ( QPainter& p );
    void drawRests        ( QPainter& p );
    void drawDeformRods   ( QPainter& p );
    void drawNx           ( QPainter& p );
    void drawSx           ( QPainter& p );
    void drawUx           ( QPainter& p );
    void paintEvent       ( QPaintEvent* e );
    void mouseMoveEvent   ( QMouseEvent* e );
    void mousePressEvent  ( QMouseEvent* e );

private:
    Backend* back;
    GraphMode mode;
    double    xMargine;
    double    yMargine;
    QVector<QPointF>  nodes;
    double lineX;
};

#endif // LITECANVAS_H
