#include "proc.h"
#include <QtMath>
#include <iostream>
#include <QMessageBox>
#include <iomanip>
#include <QDebug>

using namespace std;

Proc::Proc( QObject *parent ) :
    QObject( parent ),
    back   ( Backend::getInstance() )
{
}

void Proc::solve(){
    QVector<double> cf = back->getCforces();
    QVector<double> d = back->getDforces();
    QVector<Rod> r = back->getRods();
    QVector<QVector<double>> A = QVector<QVector<double>>(r.size()+1,
                                                          QVector<double>(r.size()+1, 0));
    QVector<double> B = QVector<double>(A.size(), 0);
    back->getU() = QVector<double>(B.size());
    for( int i = 0; i < r.size(); i++ ){
        double val = r[i].A * r[i].E / r[i].L;
        A[i][i]     += val;
        A[i][i+1]   -= val;
        A[i+1][i]   -= val;
        A[i+1][i+1] += val;
    }
    B[0]          = cf[0]          + (d[0]*r[0].L)/2.;
    B[B.size()-1] = cf[cf.size()-1] + (d[d.size()-1]*r[r.size()-1].L)/2.;
    for( int i = 1; i < r.size(); i++ ){
        B[i] = cf[i] + (d[i]*r[i].L) / 2. + (d[i-1]*r[i-1].L) / 2.;
    }

    if ( back->getlRest() ){
        A[0][0] = 1.;
        A[0][1] = 0;
        A[1][0] = 0;
        B.first() = 0;
    }
    if ( back->getrRest() ){
        int n = A.size()-1;
        A[n]  [n]   = 1.;
        A[n-1][n]   = 0;
        A[n]  [n-1] = 0;
        B.last() = 0;
    }
    QVector<QVector<double>> T = A;
    int n = T.size();
    QVector<double> a;
    QVector<double> b;
    QVector<double> c;
    QVector<double> f = B;
    a.append(0);
    for(int i = 0; i < n; i++){
        c.append(T[i][i]);
    }
    for(int i = 0; i < n-1; i++){
        b.append(T[i][i+1]);
    }
    for(int i = 1; i < n; i++){
        a.append(T[i][i-1]);
    }
    QVector<double>& U = back->getU();
    double m;
    for (int i = 1; i < n; i++)
    {
        m = a[i]/c[i-1];
        c[i] = c[i] - m*b[i-1];
        f[i] = f[i] - m*f[i-1];
    }

    U[n-1] = f[n-1]/c[n-1];

    for (int i = n - 2; i >= 0; i--)
    {
        U[i]=(f[i]-b[i]*U[i+1])/c[i];
    }
    cout<<"-----U------\n";
    for(int i = 0; i < B.size(); i++){
        cout<< back->getU()[i]<< ", ";
    }
    cout<<endl;
    emit finished();
    QMessageBox::information( nullptr, "Ok","Расчет выполнен успешно" );
}
Proc::~Proc()
{
}
