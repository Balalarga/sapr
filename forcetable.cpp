#include "forcetable.h"

ForceTable::ForceTable( QWidget* parent ):
    MyTableWidget( parent )
{
    verticalHeader()->hide();
    setColumnCount(2);
    connect( this, SIGNAL( cellChanged(int, int) ), this, SLOT(m_onCellChanged(int, int)) );
}
void ForceTable::clearAll(){
    for( int i = 0; i < rowCount(); i++ ){
        item( i, 1 )->setText( defaultValue );
    }
}

void ForceTable::addRow()
{
    setRowCount( rowCount() + 1 );
    //First column - counter
    auto item0 = this->item( rowCount()-1, 0 );
    if( !item0 ){
        item0 = new QTableWidgetItem();
        item0->setTextAlignment( textAlign );
        setItem( rowCount()-1, 0, item0 );
        item0->setText( QString::number(rowCount()) );
    }else
        item0->setText( QString::number(rowCount()) );
    item0->setFlags(item0->flags() ^ Qt::ItemFlag::ItemIsEditable ^ Qt::ItemFlag::ItemIsSelectable);

    //Second column - value
    auto item1 = this->item( rowCount()-1, 1 );
    if( !item1 ){
        item1 = new QTableWidgetItem( defaultValue );
        item1->setTextAlignment( textAlign );
        setItem( rowCount()-1, 1, item1 );
    }else
        item1->setText( defaultValue );
}


void ForceTable::m_onCellChanged( int r, int c ){
    bool ok;
    QTableWidgetItem* item = this->item( r, c );
    double value = item->text().toDouble( &ok );
    if( ok ){
        item->setBackgroundColor( goodColor );
        emit cellChangedAt( r, c, value );
    } else {
        item->setBackgroundColor( badColor );
    }
}
