#include "rodstable.h"


RodsTable::RodsTable(QWidget* parent):
    MyTableWidget(parent)
{
    setDefaultValue("1");
    setColumnCount( 4 );
    setHorizontalHeaderLabels( QStringList() << "Длина" << "Сечение" << "Е" << "\u03C3");
    connect( this, SIGNAL( cellChanged(int, int) ), this, SLOT(m_onCellChanged(int, int)) );
}

void RodsTable::clearAll(){
    for( int i = 0; i < rowCount(); i++ ){
        for( int j = 0; j < columnCount(); j++ ){
            item( i, j )->setText( defaultValue );
        }
    }
}

Rod RodsTable::getLastRod()
{
    Rod r;
    r.L =    item( rowCount()-1, 0 )->text().toDouble();
    r.A =    item( rowCount()-1, 1 )->text().toDouble();
    r.E =    item( rowCount()-1, 2 )->text().toDouble();
    r.maxF = item( rowCount()-1, 3 )->text().toDouble();
    return r;
}

void RodsTable::m_onCellChanged( int r, int c ){
    bool ok;
    QTableWidgetItem* item = this->item( r, c );
    double value = item->text().toDouble( &ok );
    if( ok && value > 0 ){
        item->setBackgroundColor( goodColor );
        emit cellChangedAt( r, c, value );
    } else {
        item->setBackgroundColor( badColor );
    }
}
