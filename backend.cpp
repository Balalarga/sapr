#include "backend.h"

Backend* Backend::p_instance = 0;

Backend* Backend::getInstance(){
    if(!p_instance)
        p_instance = new Backend;
    return p_instance;
}
Backend::Backend(){
    rods = new QVector<Rod>;
    cforces = new QVector<double>;
    dforces = new QVector<double>;
    U = new QVector<double>;
    lRest = true;
    rRest = false;
}
Backend::Backend(const Backend&){
}
Backend& Backend::operator=(Backend&){
    return *p_instance;
}
QVector<Rod>& Backend::getRods(){
    return *rods;
}
QVector<double>& Backend::getCforces(){
    return *cforces;
}
QVector<double>& Backend::getDforces(){
    return *dforces;
}
QVector<double>& Backend::getU(){
    return *U;
}
bool& Backend::getlRest(){
    return lRest;
}
bool& Backend::getrRest(){
    return rRest;
}
