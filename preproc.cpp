#include "preproc.h"
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QMenuBar>
#include <QMessageBox>

Preproc::Preproc( QWidget *parent )
    : QMainWindow    ( parent ),
      back           ( Backend::getInstance() ),
      mainWidget     ( new QWidget ),
      mainl          ( new QGridLayout(mainWidget) ),
      ctrll          ( new QVBoxLayout ),
      restl          ( new QHBoxLayout ),
      rodsCounter    ( new QSpinBox ),
      rodsLabel      ( new QLabel("\nСтержни") ),
      cforcesLabel   ( new QLabel("\nСосредоточенные нагрузки") ),
      dforcesLabel   ( new QLabel("\nРаспределенные нагрузки") ),
      restLabel      ( new QLabel("Заделки") ),
      lRestCB        ( new QCheckBox("Левая") ),
      rRestCB        ( new QCheckBox("Правая") ),
      clearBtn       ( new QPushButton("Очистить", this) ),
      compBtn        ( new QPushButton("Расчёт", this) ),
      canvas         ( new Canvas(this) ),
      rodsTable      ( new RodsTable ),
      cforcesTable   ( new ForceTable ),
      dforcesTable   ( new ForceTable ),
      warningCounter ( 0 )
{
    setup();
    resize( 800, 500 );
    lRestCB->setCheckState( Qt::Checked );
    rRestCB->setCheckState( Qt::Unchecked );
}

void Preproc::setup(){
    //Menu
    fileMenu = this->menuBar()->addMenu( "&File" );
    fileMenu->addAction   ( "Open",  this, SLOT(openFile()), Qt::CTRL + Qt::Key_O );
    fileMenu->addAction   ( "Save",  this, SLOT(saveFile()), Qt::CTRL + Qt::Key_S );
    fileMenu->addSeparator();
    fileMenu->addAction   ( "About", this, SLOT(getInfo())      /*No key*/        );
    fileMenu->addSeparator();
    fileMenu->addAction   ( "Exit",  this, SLOT(s_exit()),   Qt::ALT + Qt::Key_F4 );

    //Connecting
    connect( rodsCounter, SIGNAL(valueChanged (int)),             this,  SLOT(counterChanged(int))             );
    connect( rodsTable,   SIGNAL(cellChangedAt(int, int, double)),this,  SLOT(rodChanged    (int, int, double)));
    connect( cforcesTable,SIGNAL(cellChangedAt(int, int, double)),this,  SLOT(cforceChanged (int, int, double)));
    connect( dforcesTable,SIGNAL(cellChangedAt(int, int, double)),this,  SLOT(dforceChanged (int, int, double)));
    connect( this,        SIGNAL(updated      ()),                canvas,SLOT(update        ())                );
    connect( clearBtn,    SIGNAL(clicked      ()),                this,  SLOT(clearScreen   ())                );
    connect( compBtn,     SIGNAL(clicked      ()),                this,  SLOT(compRods      ())                );
    connect( lRestCB,     SIGNAL(stateChanged (int)),             this,  SLOT(lRestChanged  (int))             );
    connect( rRestCB,     SIGNAL(stateChanged (int)),             this,  SLOT(rRestChanged  (int))             );
    connect( canvas,      SIGNAL(deleteRod    (int)),             this,  SLOT(deleteRod     (int))             );
    connect( canvas,      SIGNAL(addRodAfter  (int)),             this,  SLOT(addRodAfter   (int))             );
    connect( canvas,      SIGNAL(addRodBefor  (int)),             this,  SLOT(addRodBefor   (int))             );

    //Tables
    cforcesTable->setHorizontalHeaderLabels( QStringList()<< "Узел" << "Fx" );
    cforcesTable->setDefaultValue          ( "0" );
    cforcesTable->addRow                   ();
    dforcesTable->setHorizontalHeaderLabels( QStringList()<< "Стержень" << "qx" );
    dforcesTable->setDefaultValue          ( "0" );

    //SpinBox
    rodsCounter->setValue  ( 1 );
    rodsCounter->setMinimum( 1 );

    //Layouts
    setCentralWidget( mainWidget );
    mainl->addWidget( canvas , 0, 0 );
    mainl->addLayout( ctrll, 0, 1 );
    ctrll->addWidget( restLabel );
    restl->addWidget( lRestCB );
    restl->addWidget( rRestCB );
    ctrll->addLayout( restl );
    ctrll->addWidget( rodsLabel );
    ctrll->addWidget( rodsCounter );
    ctrll->addWidget( rodsTable );
    ctrll->addWidget( cforcesLabel );
    ctrll->addWidget( cforcesTable );
    ctrll->addWidget( dforcesLabel );
    ctrll->addWidget( dforcesTable );
    ctrll->addWidget( clearBtn );
    ctrll->addWidget( compBtn );
}

//Slot
void Preproc::clearScreen(){
    rodsCounter->setValue( 1 );
    rodsTable->clearAll();
    dforcesTable->clearAll();
    cforcesTable->clearAll();
    back->getU().clear();
}
void Preproc::compRods(){
    emit compute();
}
void Preproc::s_exit()
{
    exit(0);
}
void Preproc::lRestChanged( int state ){
    if ( state == Qt::Unchecked && rRestCB->isChecked() == Qt::Unchecked ){
        lRestCB->setCheckState( Qt::Checked );
        warningCounter++;
        if( warningCounter == 1 )
            QMessageBox::warning(this, "Но, но, но!!!", "Нельзя убрать все заделки!\n"
                                                        "(1/4 ошибок)", QMessageBox::Close);
        else if( warningCounter == 2 )
            QMessageBox::warning(this, "ЭЙ!!!", "Говорю же, нельзя!!\n(2/4 ошибок)", QMessageBox::Close);
        else if( warningCounter == 3 )
            QMessageBox::warning(this, "Последняя капля!!!", "Ну это уже перебор\n"
                                                             "В следующий раз закрою программу!!!!\n(3/4 ошибок)",
                                 QMessageBox::Close);
        else s_exit();
    }else{
        back->getlRest() = state == Qt::Checked;
        emit updated();
    }
}
void Preproc::rRestChanged( int state ){
    if ( state == Qt::Unchecked && lRestCB->isChecked() == Qt::Unchecked ){
        rRestCB->setCheckState( Qt::Checked );
        warningCounter++;
        if( warningCounter == 1)
            QMessageBox::warning(this, "Но, но, но!!!", "Нельзя убрать все заделки!"
                                                        "\n(1/4 ошибок)", QMessageBox::Close);
        else if( warningCounter == 2)
            QMessageBox::warning(this, "ЭЙ!!!", "Говорю же, нельзя убрать все заделки!!"
                                                "\n(2/4) ошибок", QMessageBox::Close);
        else if( warningCounter == 3)
            QMessageBox::warning(this, "Последняя капля!!!", "Ну это уже перебор\n"
                                                             "В следующий раз закрою программу!!!!\n"
                                                             "(3/4 ошибок)",
                                 QMessageBox::Close);
        else s_exit();
    }else{
        back->getrRest() = state == Qt::Checked;
        emit updated();
    }
}

void Preproc::deleteRod( int n ){
    if( back->getRods().size() == 1 )
        return;

    for( int i = n; i < back->getRods().size()-1; i++ ){
        back->getRods()[i] = back->getRods()[i+1];
    }
    back->getRods().removeLast();
    for( int i = n; i < back->getCforces().size()-1; i++ ){
        back->getCforces()[i] = back->getCforces()[i+1];
    }
    back->getCforces().removeLast();
    for( int i = n; i < back->getDforces().size()-1; i++ ){
        back->getDforces()[i] = back->getDforces()[i+1];
    }
    back->getDforces().removeLast();

    rodsCounter->setValue( back->getRods().size() );
    refillTables();
    emit updated();
}
void Preproc::addRodAfter( int n ){
    if( n == back->getRods().size()-1 ){
        rodsCounter->stepUp();
        return;
    }
    addRodBefor( n+1 );
}
void Preproc::addRodBefor( int n ){
    rodsCounter->stepUp();

    Rod t = back->getRods().last();
    for( int i = back->getRods().size()-1; i > n; i-- ){
        back->getRods()[ i ] = back->getRods()[ i-1 ];
    }
    back->getRods()[ n ] = t;

    double tf = back->getCforces().last();
    for( int i = back->getCforces().size()-1; i > n; i-- ){
        back->getCforces()[ i ] = back->getCforces()[ i-1 ];
    }
    back->getCforces()[ n ] = tf;

    tf = back->getDforces().last();
    for( int i = back->getDforces().size()-1; i > n; i-- ){
        back->getDforces()[ i ] = back->getDforces()[ i-1 ];
    }
    back->getDforces()[ n ] = tf;

    refillTables();
    emit updated();
}

void Preproc::saveFile(){
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save construction"),
                                                    "", tr("Construction(*.cntr)"));
    if( !fileName.contains(".cntr") ){
        fileName += ".cntr";
    }
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly)){
        return;
    }
    QDataStream out(&file);
    out << back->getRods().size()
        << back->getRods()
        << back->getCforces()
        << back->getDforces()
        << back->getlRest()
        << back->getrRest();
}
void Preproc::openFile(){
    QString fileName = QFileDialog::getOpenFileName(this, tr("Load constriction"),
                                                    "", tr("Construction(*.cntr)"));
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)){
        return;
    }
    QDataStream in(&file);
    int len;
    in >> len;
    rodsCounter->setValue(len);
    in >> back->getRods();
    in >> back->getCforces();
    in >> back->getDforces();
    bool f;
    in >> f;
    lRestCB->setCheckState(f ? Qt::Checked : Qt::Unchecked);
    back->getlRest() = f;
    in >> f;
    rRestCB->setCheckState(f ? Qt::Checked : Qt::Unchecked);
    back->getrRest() = f;
    refillTables();
    emit updated();
}
void Preproc::getInfo()
{
}

void Preproc::counterChanged( int n ){
    if( n > rodsTable->rowCount() ){
        while(rodsTable->rowCount() < n){
            rodsTable->addRow();
            cforcesTable->addRow();
            dforcesTable->addRow();
            Rod r = rodsTable->getLastRod();
            back->getRods().append( r );
            if( n == 1 )
                back->getCforces().append( 0 );
            back->getCforces().append( 0 );
            back->getDforces().append( 0 );
        }
    } else {
        for( back->getRods().size(); back->getRods().size() > n; ){
            back->getRods().removeLast();
            back->getDforces().removeLast();
        }
        for( back->getCforces().size(); back->getCforces().size() > n+1; ){
            back->getCforces().removeLast();
        }
        rodsTable->setRowCount( n );
        cforcesTable->setRowCount( n+1 );
        dforcesTable->setRowCount( n );
    }
    emit updated();
}

void Preproc::rodChanged( int r, int c, double val ){
    if( r < back->getRods().size() ){
        if( c == 0 )
            back->getRods()[ r ].L = val;
        else if( c == 1 )
            back->getRods()[ r ].A = val;
        else if(c == 2)
            back->getRods()[ r ].E = val;
        else
            back->getRods()[ r ].maxF = val;
        emit updated();
    }
}
void Preproc::cforceChanged( int r, int c, double val ){
    if( r < back->getCforces().size() ){
        back->getCforces()[ r ] = val;
        emit updated();
    }
}
void Preproc::dforceChanged( int r, int c, double val ){
    if( r < back->getDforces().size() ){
        back->getDforces()[ r ] = val;
        emit updated();
    }
}
void Preproc::resizeEvent(QResizeEvent* e){
    canvas->setFixedWidth(mainWidget->width()*.62);
}

void Preproc::refillTables(){
    for( int i = 0; i < rodsTable->rowCount(); i++ ){
        rodsTable->item( i, 0 )->setText( QString::number(back->getRods()[i].L) );
        rodsTable->item( i, 1 )->setText( QString::number(back->getRods()[i].A) );
        rodsTable->item( i, 2 )->setText( QString::number(back->getRods()[i].E) );
        rodsTable->item( i, 3 )->setText( QString::number(back->getRods()[i].maxF) );
    }
    for( int i = 0; i < cforcesTable->rowCount(); i++ ){
        cforcesTable->item( i, 1 )->setText( QString::number(back->getCforces()[i]) );
    }
    for( int i = 0; i < dforcesTable->rowCount(); i++ ){
        dforcesTable->item( i, 1 )->setText( QString::number(back->getDforces()[i]) );
    }
}

Preproc::~Preproc()
{

}
