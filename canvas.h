#ifndef CANVAS_H
#define CANVAS_H

#include <QWidget>
#include <QPainter>
#include <QMouseEvent>
#include <QMenu>

#include "backend.h"
#include "types.h"

class Canvas : public QWidget
{
    Q_OBJECT
public:
    Canvas( QWidget* parent = nullptr );

signals:
    void deleteRod  ( int n );
    void addRodAfter( int n );
    void addRodBefor( int n );


public slots:

protected slots:
    void contextMenu      ( QAction* a );
    void setCforcesVisible();
    void setDforcesVisible();
    void sDeleteRod       ();
    void sAddRodAfter     ();
    void sAddRodBefor     ();

protected:
    void paintEvent      ( QPaintEvent* e );
    void contextMenuEvent( QContextMenuEvent* e);

    void drawRods        ( QPainter& p );
    void drawForces      ( QPainter& p );
    void drawRests       ( QPainter& p );

    double getModify     ( double param );
    double getMaxA       ();
    double getSumL       ();

    Backend* back;
    QMenu*           menu;
    QAction*         cfAct;
    QAction*         dfAct;
    QPoint           menuPoint;
    QVector<QPoint>  nodes;
    double           xMargine;
    double           yMargine;
    bool             cforcesValuesVisible;
    bool             dforcesValuesVisible;
};

#endif // CANVAS_H
