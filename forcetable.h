#ifndef FORCETABLE_H
#define FORCETABLE_H

#include "mytablewidget.h"

class ForceTable : public MyTableWidget
{
    Q_OBJECT
public:
    ForceTable( QWidget* parent = nullptr );
    void clearAll();

protected slots:
    void m_onCellChanged( int r, int c );

public slots:
    void addRow();
};

#endif // FORCETABLE_H
