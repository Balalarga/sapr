#ifndef PROC_H
#define PROC_H

#include <QObject>
#include <QVector>

#include "backend.h"
#include "types.h"

class Proc : public QObject
{
    Q_OBJECT
public:
    explicit Proc( QObject *parent = nullptr );
    ~Proc();
    void setlRest(bool l);
    void setrRest(bool r);

signals:
    void finished();

public slots:
    void solve();

private:
    Backend* back;
};

#endif // PROC_H
