#-------------------------------------------------
#
# Project created by QtCreator 2018-12-05T12:18:06
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = SAPR_MAIN
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
    canvas.cpp \
    forcetable.cpp \
    mytablewidget.cpp \
    preproc.cpp \
    rodstable.cpp \
    types.cpp \
    MainWindow.cpp \
    proc.cpp \
    postproc.cpp \
    litecanvas.cpp \
    backend.cpp

HEADERS += \
    canvas.h \
    forcetable.h \
    mytablewidget.h \
    preproc.h \
    rodstable.h \
    types.h \
    MainWindow.h \
    proc.h \
    postproc.h \
    litecanvas.h \
    backend.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

SUBDIRS += \
    SAPR_MAIN.pro

DISTFILES +=
