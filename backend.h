#ifndef BACKEND_H
#define BACKEND_H

#include "types.h"
#include <QVector>

class Backend{
private:
    static Backend* p_instance;
    Backend();
    Backend(const Backend&);
    Backend& operator=(Backend&);

    QVector<Rod>*    rods;
    QVector<double>* cforces;
    QVector<double>* dforces;
    QVector<double>* U;
    bool             lRest;
    bool             rRest;

public:
    QVector<Rod>&    getRods();
    QVector<double>& getCforces();
    QVector<double>& getDforces();
    QVector<double>& getU();
    bool&            getlRest();
    bool&            getrRest();
    static Backend*  getInstance();
};

#endif // BACKEND_H
