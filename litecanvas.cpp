#include "litecanvas.h"
#include <QDebug>
#include <QMouseEvent>

LiteCanvas::LiteCanvas( GraphMode m, QWidget *parent ) :
    QWidget ( parent ),
    back    ( Backend::getInstance() ),
    mode    ( m ),
    xMargine( 50. ),
    yMargine( 50. ),
    lineX   ( yMargine )
{
}
LiteCanvas::~LiteCanvas()
{
}

void LiteCanvas::setMode(GraphMode m)
{
    mode = m;
    update();
}
void LiteCanvas::paintEvent( QPaintEvent *e )
{
    QPainter p(this);
    drawRods(p);
    if( 0 < back->getU().size() ){
        if( mode == Mode_Nx )
            drawNx(p);
        else if( mode == Mode_Ux )
            drawUx(p);
        else
            drawSx(p);
    }
    drawRods(p);
    drawRests(p);
    p.setPen( QPen(QBrush(Qt::black), 2) );
    p.drawLine( lineX, yMargine*0.5, lineX, width()-yMargine*0.5 );
}


void LiteCanvas::mouseMoveEvent( QMouseEvent* e ){
    if( e->x() < xMargine || e->x() > width()-xMargine )
        return;
    lineX = e->x();
    double x = (lineX-xMargine)*getSumL()/(width()-2*xMargine);
    emit lineMoved(x);
    update();
}
void LiteCanvas::mousePressEvent( QMouseEvent* e ){
    if( e->x() < xMargine || e->x() > width()-xMargine )
        return;
    if(e->button() == Qt::LeftButton){
        lineX = e->x();
    }
    double x = (lineX-xMargine)*getSumL()/(width()-2*xMargine);
    emit lineMoved(x);
    update();
}

void LiteCanvas::setLineX(double x ){
    update();
}

double LiteCanvas::getSumL(){
    double sumL = 0;
    for( int i = 0; i < back->getRods().size(); i++ ){
        sumL += back->getRods().at(i).L;
    }
    return sumL;
}
double LiteCanvas::getMaxA(){
    double maxA = 8;
    for( int i = 0; i < back->getRods().size(); i++ ){
        if( back->getRods().at(i).A > maxA )
            maxA = back->getRods().at(i).A;
    }
    return maxA;
}
double LiteCanvas::getModify( double param ){
    return param;
}

void LiteCanvas::drawRods( QPainter& p ){
    if( back->getRods().isEmpty() )
        return;
    p.setPen( QPen(QBrush(Qt::SolidLine), 1) );
    int x = xMargine;
    int y = yMargine;
    int maxA = getMaxA();
    int sumL = getSumL();
    int stepX = ( width()-2*xMargine )/sumL;
    int stepY = ( height()-2*yMargine )/maxA;
    while( back->getRods().size()+1 < nodes.size() ){
        nodes.removeLast();
    }
    while( nodes.size() < back->getRods().size()+1 ){
        nodes.append( QPointF(0, 0) );
    }
    for( int i = 0; i < back->getRods().size(); i++ ){
        int offset  = stepY*( maxA - back->getRods().at(i).A )/2;
        int len = getModify( back->getRods().at(i).L) * stepX;
        int area = getModify( back->getRods().at(i).A) * stepY;
        p.drawRect( x , (y + offset), len, area );
        nodes[ i ].setX( x );
        nodes[ i ].setY( y + offset + area/2 );
        x += len;
    }
    nodes.last().setX( x );
    nodes.last().setY( nodes[0].y() );
    p.setPen( QPen(QBrush(Qt::SolidLine), 3) );
    p.drawLine(nodes.first(), nodes.last());
}

void LiteCanvas::drawRests( QPainter &p )
{
    int lines = 10;
    int scale = 5;
    int lineHeight = ( height()-2*yMargine )/scale;
    int step = lineHeight/lines;
    p.setPen( QPen(QBrush(Qt::SolidLine), 1) );
    if( back->getlRest() ){
        QPointF p1( nodes.first().x(), nodes.first().y() + lineHeight/2 );
        QPointF p2( nodes.first().x(), nodes.first().y() - lineHeight/2 );
        p.drawLine(p1, p2);
        for( int i = 0; i < lines+1; i++ ){
            QPointF t1, t2;
            t1.setX( p1.x() );
            t1.setY( p1.y()- step*i );
            t2.setX( t1.x()- step );
            t2.setY( t1.y()- step );
            p.drawLine( t1, t2 );
        }
    }
    if( back->getrRest() ){
        QPointF p1( nodes.last().x(), nodes.last().y() + lineHeight/2 );
        QPointF p2( nodes.last().x(), nodes.last().y() - lineHeight/2 );
        p.drawLine( p1, p2 );
        for( int i = 0; i < lines+1; i++ ){
            QPointF t1, t2;
            t1.setX( p1.x() );
            t1.setY( p1.y()- step*i );
            t2.setX( t1.x()+ step );
            t2.setY( t1.y()+ step );
            p.drawLine( t1, t2 );
        }
    }
}

void LiteCanvas::drawDeformRods(QPainter &p)
{
    QVector<Rod>& r = back->getRods();
    int details = 30;
    double maxA = getMaxA();
    int stepX = ( width()-2*xMargine )/getSumL();
    int stepY = ( height()-2*yMargine )/maxA;
    for( int i = 0; i < r.size(); i++ ){
        QPainterPath path;
        QPainterPath invPath;
        path.moveTo(nodes[i]);
        invPath.moveTo(nodes[i]);
        //int offset  = stepY*( maxA - r[i].A )/2;
        //p.drawRect( x , (y + offset), r[i].L, r[i].A );
        for(int j = 0; j < details; j++){

        }

        p.drawPath(path);
    }
}

double LiteCanvas::Nx( int p, double x ){
    Rod c = back->getRods()[p];
    QVector<double> u = back->getU();
    return c.A*c.E*(u[p+1] - u[p])/c.L + back->getDforces()[p]*c.L*(1-2*x/c.L) / 2.;
}
double LiteCanvas::Ux( int p, double x ){
    Rod c = back->getRods()[p];
    QVector<double> u = back->getU();
    double df = back->getDforces()[p];
    return u[p] + x*(u[p+1]-u[p])/c.L + df*c.L*x*(1-x/c.L)/(2*c.E*c.A);
}
double LiteCanvas::Sx( int p, double x ){
    Rod c = back->getRods()[p];
    return Nx(p, x)/c.A;
}

void LiteCanvas::drawNx( QPainter& p )
{
    bool ok = false;
    for(int i = 0; i < back->getCforces().size(); i++){
        if( back->getCforces()[i] != 0.){
            ok = true;
            break;
        }
    }
    for(int i = 0; i < back->getDforces().size(); i++){
        if( back->getDforces()[i] != 0.){
            ok = true;
            break;
        }
    }
    if( !ok ){
        p.drawLine(nodes.first(), nodes.last());
    }
    p.setPen( QPen(QBrush(Qt::red), 4) );
    double mid = nodes[ 0 ].y();
    QVector<QLineF> lines;
    double max = 0;
    for(int i = 0; i < back->getRods().size(); i++){
        QPointF p1;
        QPointF p2;
        p1.setX(nodes[i].x());
        p2.setX(nodes[i+1].x());
        Rod c = back->getRods()[i];
        double y1, y2;
        y1 = Nx(i, 0);
        y2 = Nx(i, c.L);
        p1.setY(y1);
        p2.setY(y2);
        if(max < qAbs(y1))
            max = qAbs(y1);
        if(max < qAbs(y2))
            max = qAbs(y2);
        lines.append(QLineF(p1,p2));
    }
    double step = ( mid-yMargine )/max;
    double prevY = mid;
    for(int i = 0; i < lines.size(); i++){
        double x1 = lines[i].p1().x();
        double y1 = mid - lines[i].p1().y()*step;
        double x2 = lines[i].p2().x();
        double y2 = mid - lines[i].p2().y()*step;
        lines[i].setP1(QPointF(x1,y1));
        lines[i].setP2(QPointF(x2,y2));
        if( prevY != y1 )
            p.drawLine(x1, y1, x1, prevY);
        prevY = y1;
        if( i == 0 || i == lines.size()-1)
            prevY = y2;
    }
    p.drawLine(nodes.last().x(), prevY, nodes.last().x(), mid);
    p.drawLines(lines);
}
void LiteCanvas::drawUx( QPainter& p ){
    QPainterPath path;
    QVector<double> ys;
    double max = 0;
    double mid = nodes[0].y();
    int details = 30;
    path.moveTo(nodes[0]);
    for(int i = 0; i < back->getRods().size(); i++){
        double step = back->getRods()[i].L/double(details);
        for(int j = 0; j < details; j++){
            double y = Ux(i, step*j);
            ys.append(y);
            if( qAbs(y) > max)
                max = qAbs(y);
        }
    }
    double stepY = (mid - yMargine)/max;

    p.setPen( QPen(QBrush(Qt::blue), 4) );
    for(int i = 0; i < back->getRods().size(); i++){
        double x = nodes[i].x();
        double stepX = (nodes[i+1].x()-nodes[i].x())/details;
        for(int j = 0; j < details; j++){
            path.lineTo(x + stepX*j, mid - ys[i*details+j]*stepY);
        }
    }
    path.lineTo(nodes.last().x(), mid - ys.last()*stepY);
    path.lineTo(nodes.last());
    p.drawPath(path);
}
void LiteCanvas::drawSx( QPainter& p )
{
    bool ok = false;
    for(int i = 0; i < back->getCforces().size(); i++){
        if( back->getCforces()[i] != 0.){
            ok = true;
            break;
        }
    }
    for(int i = 0; i < back->getDforces().size(); i++){
        if( back->getDforces()[i] != 0.){
            ok = true;
            break;
        }
    }
    if( !ok ){
        p.drawLine(nodes.first(), nodes.last());
        return;
    }
    double mid = nodes[ 0 ].y();
    QVector<QLineF> lines;
    double max = 0;
    for(int i = 0; i < back->getRods().size(); i++){
        QPointF p1;
        QPointF p2;
        p1.setX(nodes[i].x());
        p2.setX(nodes[i+1].x());
        Rod c = back->getRods()[i];
        double y1, y2;
        y1 = Sx(i, 0);
        y2 = Sx(i, c.L);
        p1.setY(y1);
        p2.setY(y2);
        if(max < qAbs(y1))
            max = qAbs(y1);
        if(max < qAbs(y2))
            max = qAbs(y2);
        if(max < qAbs(c.maxF)){
            max = qAbs(c.maxF);
        }
        lines.append(QLineF(p1,p2));
    }
    double step = ( mid-yMargine )/max;

    p.setPen( QPen(QBrush(qRgba(255, 0, 0, 50)), 4) );
    for( int i = 0; i < back->getRods().size(); i++ ){
        double y = back->getRods()[i].maxF * step;
        double wid = nodes[i+1].x()-nodes[i].x();
        p.fillRect(nodes[i].x(), mid - y, wid, y*2, QBrush(Qt::lightGray));
    }

    double prevY = mid;
    p.setPen( QPen(QBrush(Qt::blue), 4) );
    for(int i = 0; i < lines.size(); i++){
        double x1 = lines[i].p1().x();
        double y1 = mid - lines[i].p1().y()*step;
        double x2 = lines[i].p2().x();
        double y2 = mid - lines[i].p2().y()*step;
        lines[i].setP1(QPointF(x1,y1));
        lines[i].setP2(QPointF(x2,y2));
        if( prevY != y1 )
            p.drawLine(x1, y1, x1, prevY);
        prevY = y1;
        if( i == 0 || i == lines.size()-1)
            prevY = y2;
    }
    p.drawLine(nodes.last().x(), prevY, nodes.last().x(), mid);
    p.drawLines(lines);
}
