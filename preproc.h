#ifndef PREPROC_H
#define PREPROC_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QCheckBox>
#include <QMenu>
#include <QPushButton>

#include "backend.h"
#include "canvas.h"
#include "rodstable.h"
#include "forcetable.h"
#include "types.h"
#include "proc.h"

class Preproc : public QMainWindow
{
    Q_OBJECT

public:
    Preproc( QWidget *parent = nullptr );
    ~Preproc();

protected:
    void resizeEvent(QResizeEvent* e);

signals:
    void updated();
    void compute();

public slots:
    void deleteRod     ( int n );
    void addRodAfter   ( int n );
    void addRodBefor   ( int n );
    void counterChanged( int n );
    void rodChanged    ( int r, int c, double val );
    void cforceChanged ( int r, int c, double val );
    void dforceChanged ( int r, int c, double val );
    void lRestChanged  ( int state );
    void rRestChanged  ( int state );

    //Menu slots
    void openFile();
    void saveFile();
    void clearScreen();
    void compRods();
    void getInfo();
    void s_exit();

private:
    //BackEnd
    Backend* back;

    //FrontEnd
    QMenu*       fileMenu;
    QMenu*       setupMenu;
    QWidget*     mainWidget;
    QGridLayout* mainl;
    QVBoxLayout* ctrll;
    QHBoxLayout* restl;
    QSpinBox*    rodsCounter;
    QLabel*      rodsLabel;
    QLabel*      cforcesLabel;
    QLabel*      dforcesLabel;
    QLabel*      restLabel;
    QCheckBox*   lRestCB;
    QCheckBox*   rRestCB;
    QPushButton* clearBtn;
    QPushButton* compBtn;

    //CustomClasses
    Canvas*      canvas;
    RodsTable*   rodsTable;
    ForceTable*  cforcesTable;
    ForceTable*  dforcesTable;

    int warningCounter;

    //Functions
    void setup();
    void refillTables();
};

#endif // PREPROCW_H
