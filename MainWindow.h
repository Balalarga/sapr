#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QTabWidget>

#include "preproc.h"
#include "postproc.h"

class MainWindow : public QTabWidget
{
    Q_OBJECT

public slots:

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Preproc*  prep;
    Proc*     proc;
    Postproc* post;
};

#endif // MAINWINDOW_H
