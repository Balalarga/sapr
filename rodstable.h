#ifndef RODSTABLE_H
#define RODSTABLE_H

#include "mytablewidget.h"
#include "types.h"

class RodsTable : public MyTableWidget
{
    Q_OBJECT
public:
    RodsTable( QWidget* parent = nullptr );
    Rod getLastRod();
    void clearAll();

protected slots:
    void m_onCellChanged( int r, int c );

private:

};

#endif // RODSTABLE_H
