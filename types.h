#ifndef TYPES_H
#define TYPES_H

#include <QDataStream>

enum GraphMode{Mode_Nx, Mode_Ux, Mode_Sx};

class Rod{
public:
    double L;
    double A;
    double E;
    double maxF;
    friend QDataStream& operator <<( QDataStream& out, const Rod& r );
    friend QDataStream& operator >>( QDataStream& in, Rod& r );
};

#endif // TYPES_H
