#include "MainWindow.h"

MainWindow::MainWindow( QWidget *parent )
    : QTabWidget( parent ),
      prep( new Preproc (this) ),
      proc( new Proc    (this) ),
      post( new Postproc(this) )
{
    this->setTabPosition( QTabWidget::South );
    this->addTab( prep, "ПреПроцессор" );
    this->addTab( post, "ПостПроцессор" );
    connect( prep, SIGNAL(compute()), proc, SLOT(solve()) );
    connect( proc, SIGNAL(finished()), post, SLOT(updateAll()));
    resize( 1500, 800 );
}

MainWindow::~MainWindow()
{
}
