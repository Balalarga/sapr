#include "canvas.h"
#include <QDebug>
#include <QtMath>

Canvas::Canvas( QWidget *parent ):
    QWidget              ( parent ),
    back                 (Backend::getInstance()),
    menu                 ( new QMenu(this) ),
    cfAct                ( new QAction("Отображать сосредоточенные нагрузки", this) ),
    dfAct                ( new QAction("Отображать распределенные нагрузки", this) ),
    xMargine             ( 70 ),
    yMargine             ( 50 ),
    cforcesValuesVisible ( false ),
    dforcesValuesVisible ( false )
{
    connect( menu, SIGNAL(triggered(QAction*)), this, SLOT(contextMenu(QAction*)));
    cfAct->setCheckable(true);
    cfAct->setChecked(false);
    dfAct->setCheckable(true);
    dfAct->setChecked(false);

    connect( cfAct, SIGNAL(triggered()), this, SLOT(setCforcesVisible()) );
    connect( dfAct, SIGNAL(triggered()), this, SLOT(setDforcesVisible()) );

    menu->addAction( "Удалить стержень",      this, SLOT(sDeleteRod())   );
    menu->addAction( "Добавить стержень до...", this, SLOT(sAddRodBefor()) );
    menu->addAction( "Добавить стержень после...",  this, SLOT(sAddRodAfter()) );
    menu->addSeparator();
    menu->addAction( cfAct );
    menu->addAction( dfAct );
}

void Canvas::contextMenu( QAction* a ){
    //  -----------
    //  -----------
    //  ---Empty---
    //  -----------
    //  -----------
}

void Canvas::contextMenuEvent( QContextMenuEvent* e ){
    menuPoint = e->pos();
    menu->exec(e->globalPos());
}

void Canvas::sDeleteRod(){
    for( int i = 0; i < back->getRods().size(); i++ ){
        if( nodes[ i ].x() <= menuPoint.x() &&
                nodes[i+1].x() >= menuPoint.x() ){
            emit deleteRod( i );
            break;
        }
    }
}
void Canvas::sAddRodAfter(){
    for( int i = 0; i < back->getRods().size(); i++ ){
        if( nodes[ i ].x() <= menuPoint.x() &&
                nodes[i+1].x() >= menuPoint.x() ){
            emit addRodAfter( i );
            return;
        }
    }
}
void Canvas::sAddRodBefor(){
    for( int i = 0; i < back->getRods().size(); i++ ){
        if( nodes[ i ].x() <= menuPoint.x() &&
                nodes[i+1].x() >= menuPoint.x() ){
            emit addRodBefor( i );
            return;
        }
    }
}
void Canvas::setCforcesVisible(){
    cforcesValuesVisible = cfAct->isChecked();
    update();
}
void Canvas::setDforcesVisible(){
    dforcesValuesVisible = dfAct->isChecked();
    update();
}
double Canvas::getMaxA(){
    double maxA = 8;
    for( int i = 0; i < back->getRods().size(); i++ ){
        if( back->getRods()[i].A > maxA )
            maxA = back->getRods()[i].A;
    }
    return maxA;
}
double Canvas::getSumL(){
    double sumL = 0;
    for( int i = 0; i < back->getRods().size(); i++ ){
        sumL += back->getRods()[i].L;
    }
    return sumL;
}
double Canvas::getModify( double param ){
    return param;
}

//Graphics functions
void Canvas::drawRods( QPainter& p ){
    double x = xMargine;
    double y = yMargine;
    double maxA = getMaxA();
    double sumL = getSumL();
    double stepX = ( width()-2*xMargine )/sumL;
    double stepY = ( height()-2*yMargine )/maxA;
    while( back->getCforces().size() < nodes.size() ){
        nodes.removeLast();
    }
    while( nodes.size() < back->getCforces().size() ){
        nodes.append( QPoint(0, 0) );
    }
    for( int i = 0; i < back->getRods().size(); i++ ){
        double offset = stepY*( maxA - back->getRods()[i].A )/2;
        double len = getModify( back->getRods()[i].L) * stepX;
        double area = getModify( back->getRods()[i].A) * stepY;
        p.drawRect( x , (y + offset), len, area );
        nodes[ i ].setX( x );
        nodes[ i ].setY( y + offset + area/2 );
        x += len;
    }
    nodes.last().setX( x );
    nodes.last().setY( nodes[0].y() );
}
void Canvas::drawForces( QPainter& p ){
    int cd = 15;// Ширина стрелки сосредоточенной нагрузки
    int dd = 7;// Ширина стрелки распределенной нагрузки
    // Отрисовка сосредоточенных нагрузок
    for( int i = 0; i < back->getCforces().size(); i++ ){
        //int lineWidth = ( cforces[i]/10 )+ 3;
        int lineWidth = 3;
        p.setPen( QPen(QBrush(Qt::black), lineWidth) );
        int mid = nodes[i].y();
        if( back->getCforces()[ i ] == .0 )
            continue;
        QPoint textPoint;
        QPoint p1( nodes[ i ].x(), mid );
        QPoint p2, p3, p4;
        if( back->getCforces()[ i ] > 0 ){
            if( i == back->getCforces().size()-1 )
                p2.setX( nodes[ i ].x() + xMargine/2 );
            else
                p2.setX( nodes[ i ].x() + (nodes[i+1].x() - nodes[i].x())/2 - cd );
            p3.setX( p2.x() - cd );
        }else{
            if( i == 0 )
                p2.setX( xMargine/2 );
            else
                p2.setX( nodes[i].x() - (nodes[i].x() - nodes[i-1].x())/2 + cd );
            p3.setX( p2.x() + cd );
        }
        p2.setY( mid );
        p3.setY( mid + cd );
        p4.setY( mid - cd );
        p4.setX( p3.x() );
        QVector<QLine> arrow;
        arrow.append( QLine(p1, p2) );
        arrow.append( QLine(p2, p3) );
        arrow.append( QLine(p2, p4) );
        p.drawLines( arrow );
        textPoint.setY( mid - cd );
        if( p1.x() > p2.x() )
            textPoint.setX( p1.x() - (p1.x() - p2.x())/2 );
        else textPoint.setX( p1.x() + (p2.x() - p1.x())/2  );
        QString value;
        value.setNum( back->getCforces()[i] );
        if( cforcesValuesVisible ){
            p.setPen( Qt::darkCyan );
            p.drawText(textPoint, value);
        }
    }

    //Отрисовка распределенных нагрузок
    int arrCount = 10;
    for( int i = 0; i < back->getDforces().size(); i++ ){
        //int lineWidth = (dforces[i] / 10)+2;
        int lineWidth = 2;
        p.setPen( QPen(QBrush(Qt::black), lineWidth) );
        if( back->getDforces()[i] == 0. )
            continue;
        QVector<QPoint> baseP(arrCount);
        for( int j = 0; j < arrCount; j++ ){
            QPoint p;
            p.setY( nodes[i].y() );
            p.setX( nodes[i].x() + j*(nodes[i+1].x() - nodes[i].x())/arrCount );
            baseP[ j ] = p;
        }
        baseP.append( nodes[i+1] );
        for( int j = 0; j < baseP.size()-1; j++ ){
            QVector<QLine> arrow(3);
            QPoint tempP;
            if( back->getDforces()[i] > 0 ){
                tempP.setX( baseP[j+1].x() - dd );
                arrow[0] = QLine( baseP[j], baseP[j+1] );
            }else{
                tempP.setX( baseP[j].x() + dd );
                arrow[0] = QLine( baseP[j+1], baseP[j] );
            }
            tempP.setY( baseP[j+1].y() - dd );
            arrow[1] = QLine( arrow[0].p2(), tempP );
            tempP.setY( baseP[j+1].y() + dd );
            arrow[2] = QLine( arrow[0].p2(), tempP );
            p.drawLines( arrow );
        }
        if ( dforcesValuesVisible ){
            QPoint textPoint;
            textPoint.setX( baseP.first().x()+qAbs(baseP.last().x() - baseP.first().x())/2 );
            textPoint.setY( baseP[0].y() - dd );
            p.setPen( Qt::darkRed );
            p.drawText( textPoint, QString::number(back->getDforces()[i]) );
        }
    }
}
void Canvas::drawRests( QPainter& p ){
    if( !back->getrRest() && !back->getlRest() )
        return;
    int lines = 10;
    int scale = 5;
    int lineHeight = ( height()-2*yMargine )/scale;
    int step = lineHeight/lines;
    p.setPen( QPen(QBrush(Qt::SolidLine), 4) );
    if( back->getlRest() ){
        QPoint p1(nodes.first().x(), nodes.first().y() + lineHeight/2);
        QPoint p2(nodes.first().x(), nodes.first().y() - lineHeight/2);
        p.drawLine(p1, p2);
        for( int i = 0; i < lines+1; i++ ){
            QPoint t1, t2;
            t1.setX( p1.x() );
            t1.setY( p1.y()- step*i );
            t2.setX( t1.x()- step );
            t2.setY( t1.y()- step );
            p.drawLine( t1, t2 );
        }
    }
    if( back->getrRest() ){
        QPoint p1(nodes.last().x(), nodes.last().y() + lineHeight/2);
        QPoint p2(nodes.last().x(), nodes.last().y() - lineHeight/2);
        p.drawLine(p1, p2);
        for( int i = 0; i < lines+1; i++ ){
            QPoint t1, t2;
            t1.setX( p1.x() );
            t1.setY( p1.y()- step*i );
            t2.setX( t1.x()+ step );
            t2.setY( t1.y()+ step );
            p.drawLine( t1, t2 );
        }
    }
}

void Canvas::paintEvent( QPaintEvent *e )
{
    QPainter p( this );
    drawRods  ( p );
    drawForces( p );
    drawRests ( p );
}
