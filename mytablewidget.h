#ifndef MYTABLEWIDGET_H
#define MYTABLEWIDGET_H

#include <QTableWidget>
#include <QHeaderView>

class MyTableWidget : public QTableWidget
{
    Q_OBJECT
public:
    explicit MyTableWidget( QWidget* parent = nullptr );
    void setBadColor      ( QColor color );
    void setGoodColor     ( QColor color );
    void setDefaultValue  ( QString value );
    void setTextAlign     ( Qt::AlignmentFlag align );

signals:
    void cellChangedAt( int r, int c, double value );

public slots:
    void addRow();

protected slots:
    //void m_onCellChanged( int r, int c );

protected:
    QColor            badColor;
    QColor            goodColor;
    QString           defaultValue;
    Qt::AlignmentFlag textAlign;
};

#endif // MYTABLEWIDGET_H
