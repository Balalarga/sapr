#include "mytablewidget.h"

MyTableWidget::MyTableWidget( QWidget* parent ):
    QTableWidget( parent ),
    badColor    ( qRgba(255, 0, 0, 180) ),
    goodColor   ( qRgba(255,255,255,255) ),
    textAlign   ( Qt::AlignCenter )
{
    horizontalHeader()->setSectionResizeMode( QHeaderView::Stretch );
}

void MyTableWidget::setBadColor( QColor color ){
    badColor = color;
}
void MyTableWidget::setGoodColor( QColor color ){
    goodColor = color;
}
void MyTableWidget::setDefaultValue( QString value )
{
    defaultValue = value;
}
void MyTableWidget::setTextAlign( Qt::AlignmentFlag align ){
    textAlign = align;
}

void MyTableWidget::addRow(){
    setRowCount( rowCount() + 1 );
    for( int i = 0; i < columnCount(); i++ ){
        auto item = this->item( rowCount()-1, i );
        if( !item ){
            item = new QTableWidgetItem( defaultValue );
            item->setTextAlignment( textAlign );
            setItem( rowCount()-1, i, item );
        }else
            item->setText( defaultValue );
    }
}

